<?php
/**
 * Loop
 *
 * This is the default loop file, containing the looping logic for use in all templates
 * where a loop is required.
 *
 * To override this loop in a particular context (in all archives, for example), create a
 * duplicate of this file and rename it to `loop-archive.php`. Make any changes to this
 * new file and they will be reflected on all your archive screens.
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $more; $more = 0;

woo_loop_before();
 query_posts(array('cat=106','posts_per_page' => 3,));
if (have_posts()) { $count = 0;
?>

<div class="fix"></div>
    <h3 class="featured-home"> Featured News </h3>
<?php
    while (have_posts()) { the_post(); $count++;

        woo_get_template_part( 'content', get_post_type() );

    } // End WHILE Loop
} else {
    get_template_part( 'content', 'noposts' );
} // End IF Statement

woo_loop_after();
wp_reset_query();
?>
<div class="home-blog-lower">
    <a href="/blog" class="btn-blog"> ISES Denver Blog</a>
</div><!--end home-blog-lower -->