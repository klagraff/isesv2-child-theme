<?php
/**
 * Template Name: Events
 *
 * The blog page template displays the "blog-style" template on a sub-page.
 *
 * @package WooFramework
 * @subpackage Template
 */
$title_before = '<h1 class="title entry-title">';
$title_after = '</h1>';
 get_header();
 global $woo_options;
?>
    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div id="content" class="col-full">
        <div id="main-sidebar-container">

            <!-- #main Starts -->
            <?php woo_main_before(); ?>

            <section id="main" class="col-left">
			<header>
		<?php the_title( $title_before, $title_after ); ?>
	</header>
            <?php woo_sidebar('events'); ?>

            </section><!-- /#main -->
            <?php woo_main_after(); ?>
		 <?php get_sidebar(); ?>
        </div><!-- /#main-sidebar-container -->
	<?php get_sidebar( 'alt' ); ?>
    </div><!-- /#content -->
    <?php woo_content_after(); ?>

<?php get_footer(); ?>
